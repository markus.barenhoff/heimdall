{-# LANGUAGE OverloadedStrings #-}
module Hofund.Commands.Stats where

import Control.Monad.State.Strict   (gets)
import Data.ByteString.Char8        (ByteString)
import Data.Configurable            (serialize)
import Database.Adapter
import Hofund.Types
import Hofund.Commands.Common
import System.Console.StructuredCLI hiding (Commands)


influxAPNKey :: ByteString
influxAPNKey = "config.heimdall.stats.influx-apn"

stats :: Commands
stats =
    command "stats" "Statistics configuration" newLevel >+ do
      basic
      command "set" "Set configuration parameter" newLevel >+ do
        basic
        param "influx-apn" "Influx DB server address to export stats to" asSockAddr $ \addr -> do
          db <- gets db
          set db influxAPNKey =<< serialize addr
          return NoAction
      command "delete" "Delete a configuration parameter" newLevel >+ do
        basic
        command "influx-apn" "Delete Influx DB server address configuration" $ do
          db <- gets db
          del db [influxAPNKey]
          return NoAction