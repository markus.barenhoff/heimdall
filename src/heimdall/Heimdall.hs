{-# LANGUAGE OverloadedStrings #-}
{- |
Description: H.E.I.M.D.A.L.L - Highly Effective Intervention Manipulation and Data Analysis at the
                               Link Layer
Copyright: (c) Erick Gonzalez, 2018
License: LGPL (See LICENSE)
Maintainer: erick@codemonkeylabs.com

Passively monitors data flows according to configured rules, triggering intervention
actions to allow other network components to intercept or reroute a connection as needed.

-}
module Heimdall where

import Control.Concurrent            (ThreadId, threadDelay, myThreadId)
import Control.Monad                 (join, forever, void)
import Control.Monad.Fix             (fix)
import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Writer    (WriterT(..), execWriterT, tell)
import Control.Concurrent.Async      (Async, waitAnyCatchCancel)
import Control.Exception             (Exception(..)
                                     ,SomeException
                                     ,catch
                                     ,handle
                                     ,throwTo)
import Database.Adapter              (new)
import Heimdall.ACLs                 (runACLs)
import Heimdall.Connection.Tracking  (runTracking)
import Heimdall.Environment          (Env(..))
import Heimdall.Policies             (runPolicies)
import Heimdall.Interface.Queues     (runQueues)
import Heimdall.RouteMaps            (runRouteMaps)
import Heimdall.Interface.Tunnels    (runTunnels)
import Heimdall.Interface.VIFs       (runVIFs)
import Heimdall.Stats                (runStats)
import System.Environment            (getArgs)
import System.Posix.Signals          (Handler(..)
                                     ,SignalInfo(..)
                                     ,Signal
                                     ,installHandler
                                     ,sigINT
                                     ,sigQUIT
                                     ,sigTERM)
import System.Exit                   (exitSuccess, exitFailure)

import qualified System.Logging         as L

data ExitException = ExitException Signal
  deriving Show

instance Exception ExitException

signalsHandler :: ThreadId -> SignalInfo -> IO ()
signalsHandler mainTId info = do
  throwTo mainTId $ ExitException $ siginfoSignal info

-- | Entry point for the Heimdall service
runService :: IO ()
runService = do
  mainTId <- myThreadId
  -- sigkill cannot be intecepted
  void $ installHandler sigINT  (CatchInfoOnce $ signalsHandler mainTId) Nothing
  void $ installHandler sigQUIT (CatchInfoOnce $ signalsHandler mainTId) Nothing
  void $ installHandler sigTERM (CatchInfoOnce $ signalsHandler mainTId) Nothing
  db      <- fix $ retry new "Failure to create DB adapter: "
  args    <- parseOptions <$> getArgs
  case join $ lookup "--node" args of
    Nothing -> do
      putStrLn "Missing local node name. Specify it via the --node command argument"
      exitFailure
    Just node -> forever $ do
      let ?env = Env { getDB   = db,
                       getNode = node }
      monitors <- execWriterT $ do
                   spawn $ L.monitor db
                   spawn runTracking
                   spawn runStats
                   spawn runPolicies
                   spawn runRouteMaps
                   spawn runQueues
                   spawn runTunnels
                   spawn runVIFs
                   spawn runACLs
      handle (\(ExitException signal) -> exitBySignal signal) $ do
        s <- (snd <$> waitAnyCatchCancel monitors)
        L.errorM "Heimdall" $ "thread exits with:" ++ show s ++ " Restart."
    where retry f msg loop = f `catch` \(e::SomeException) -> do
               L.errorM "Heimdall" $ msg ++ show e
               threadDelay 2718281
               loop
          exitBySignal signal = do
            L.infoM "Heimdall" $ "Exit by signal: " <> show signal
            exitSuccess

spawn :: IO (Async ()) -> WriterT [Async ()] IO ()
spawn fn = lift fn >>= tell . pure

isOption :: String -> Bool
isOption ('-':_) = True
isOption _       = False

parseOptions :: [String] -> [(String, Maybe String)]
parseOptions args =
    parseOptions' args []

parseOptions' :: [String] -> [(String, Maybe String)] -> [(String, Maybe String)]
parseOptions' []     acc = acc
parseOptions' (x:rest@(x':xs)) acc
    | isOption x = if isOption x'
                     then parseOptions' rest ((x, Nothing):acc)
                     else parseOptions' xs ((x, Just x'):acc)
    | otherwise  = error $ "unexpected argument: " ++ x
parseOptions' [x] acc
    | isOption x = (x, Nothing):acc
    | otherwise  = error $ "unexpected trailing argument: " ++ x
