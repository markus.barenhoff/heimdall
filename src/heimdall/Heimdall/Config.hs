{- |
Description: Heimdall Configuration Interface
License: LGPL (See LICENSE)
Maintainer: erick@crankysurfer.com
Copyright: (c) Erick Gonzalez, 2018
-}

module Heimdall.Config where


import Prelude         hiding (lookup)
import Control.Exception      (Exception(..))
import Control.Monad.Except   (runExceptT)
import Control.Monad.Failable (Failable(..))
import Control.Monad.IO.Class (MonadIO)
import Database.Adapter       (Adapter(..), Error(..))
import Data.Binary            (Binary, decode)
import Data.ByteString        (ByteString)
import Data.ByteString.Lazy   (fromStrict)
import Data.Default           (Default, def)

getConfig :: (MonadIO m, Failable m, Adapter a, Default b, Binary b) => a -> ByteString -> m b
getConfig db key = do
  result <- runExceptT $ get db key
  either failed decodeConfig result
         where failed err = case fromException err of
                              Just (NotFound _)->
                                  return def
                              _ ->
                                  failure err

getConfigOr :: (MonadIO m, Failable m, Adapter a, Binary b) => a -> ByteString -> b -> m b
getConfigOr db key defValue = do
  result <- lookup db key
  maybe (return defValue) decodeConfig result

decodeConfig :: (Failable m, Binary a) => ByteString -> m a
decodeConfig = return . decode . fromStrict
