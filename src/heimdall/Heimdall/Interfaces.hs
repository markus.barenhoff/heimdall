{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Interfaces Module
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Interfaces where

import Control.Applicative           ((<|>))
import Control.Concurrent            (threadDelay)
import Control.Concurrent.Async      (Async)
import Control.Concurrent.Utils      (retryCancel)
import Control.Lens                  (set)
import Control.Monad                 ((>=>), guard, void, when, unless)
import Control.Monad.Except          (runExcept)
import Control.Monad.Failable        (Failable(..), failableIO, hoist)
import Control.Monad.IO.Class        (MonadIO, liftIO)
import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Maybe     (MaybeT(..), runMaybeT)
import Control.Exception             (finally)
import Data.ByteString.Char8         (ByteString, unpack)
import Data.Configurable             (deserialize)
import Data.IORef                    (readIORef)
import Data.Fn                       ((-.-))
import Data.List                     (sort)
import Data.Maybe                    (fromMaybe)
import Data.Referable                (Dict, Referable(..))
import Database.Adapter              (getMap)
import Heimdall.Packet
import Heimdall.Types
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel          (monitor)

import System.IO.Unsafe              (unsafePerformIO)
import System.Logging         hiding (monitor)

import qualified Data.Map as Map

type IDBs = Dict IntfName ICB

{-# NOINLINE idbs #-}
idbs :: IDBs
idbs = unsafePerformIO newDict

setIntf :: Interface -> ICB -> ICB
setIntf = set icbIntf . Just

getIDB :: (MonadIO m) => IntfName -> m IDB
getIDB = getRef idbs

getICB :: (MonadIO m, Failable m) => IntfName -> m ICB
getICB name = (getIDB >=> deRef) name >>= maybe (missingIntf name) return

updateICB :: (MonadIO m, Failable m) => IntfName -> ICB -> m ()
updateICB = void -.- updateRef idbs

modifyICB :: (MonadIO m, Failable m) => IntfName -> (ICB -> ICB) -> m ()
modifyICB = modifyRef idbs

delICB :: (MonadIO m, Failable m) => IntfName -> m ()
delICB = delRef idbs

getIntf :: (MonadIO m, Failable m) => IDB -> m (Maybe Interface)
getIntf idb = do
  mICB <- deRef idb
  return $ mICB >>= _icbIntf

getIntfM :: (MonadIO m, Failable m) => IDB -> m Interface
getIntfM idb = getIntf idb >>= maybe (missingIntf $ getRefName idb) return

missingIntf :: (Failable m) => IntfName -> m a
missingIntf = failure . UnexistingObject . show

type StartFn = IntfName -> [(ByteString, ByteString)] -> IO (Maybe (Async ()))
type StopFn  = IntfName -> IO ()

cleanupInterfaces :: (MonadIO m, Failable m) => IntfType -> (IntfName -> m ()) -> m ()
cleanupInterfaces t cleanup = do
  intfs <- (filter (\i -> t == getIntfType i) . Map.keys) <$> liftIO (readIORef idbs)
  mapM_ cleanup intfs

runInterfaces :: (?env :: Env)
              => String
              -> [ByteString]
              -> IntfType
              -> StartFn
              -> StopFn
              -> IO ()
runInterfaces component prefixes intfType runFn stopFn = do
    monitor component prefixes respawn halt `finally` cleanupInterfaces intfType (`stop` stopFn)
        where respawn key = void . runMaybeT $ do
                              (name, intfKey) <- hoist id $ getIntfRootKey key
                              newInfo         <- getInfo intfKey
                              when (key == intfKey) $ do
                                oldInfo <- _icbInfo <$> getICB name <|> pure []
                                let effectual = oldInfo /= newInfo
                                unless effectual $
                                  infoM "Interfaces.Control" $ unpack key ++ " change is non effectual. Info: "
                                    ++ show newInfo
                                guard effectual
                              infoM "Interfaces.Control" $ "(Re)starting interface " ++ show name
                              stop name stopFn
                              start name newInfo runFn
              halt key = void . runMaybeT $ do
                           (name, intfKey) <- hoist id $ getIntfRootKey key
                           info            <- getInfo intfKey
                           let intf = show name
                           infoM "Interfaces.Control" $ "Stopping interface " ++ intf
                           stop name stopFn
                           if key /= intfKey then do
                             infoM "Interfaces.Control" $ "Restarting interface " ++ intf
                             void $ start name info runFn
                           else do
                             infoM "Interfaces.Control" $ "Interface " ++ intf ++ " is now gone"
                             mapM_ delICB $ getIntfNameFromKey key
              getInfo = fmap sort . getMap (getDB ?env)

stop :: (MonadIO m, Failable m) => IntfName -> StopFn -> m ()
stop name stopFn = do
  liftIO $ stopFn name
  -- ignore any failures while stopping the interface.
  void . runMaybeT $ do
    infoM "Interfaces.Control" $ "Stopping interface " ++ show name
    ICB {..} <- getICB name
    infoM "Interfaces.Control" $ "Retrieved ICB for " ++ show name
    delICB name
    infoM "Interfaces.Control" $ "Deleted ICB for " ++ show name
    liftIO $ do
      infoM "Interfaces.Control" $ "Stopping ICB thread for " ++ show name
      void $ retryCancel _icbThread 1000000
      infoM "Interfaces.Control" $ "ICB thread for " ++ show name ++ " has been stopped"

start :: (?env::Env, MonadIO m, Failable m)
      => IntfName -> [(ByteString,ByteString)] -> StartFn -> m ()
start name info startFn = do
  let intf = show name
  infoM "Interfaces.Control" $ "Starting interface " ++ intf
  let enabledStr = fromMaybe "false" $ lookup "enabled" info
      isEnabled  = either (const False) id . runExcept $ deserialize enabledStr
  infoM "Interfaces.Control" $ "Interface " ++ intf ++ " is " ++
    (if isEnabled then "enabled" else "disabled")
  when isEnabled $ do
    thread <- (failableIO $ startFn name info) >>= maybe (interfaceStartError name) return
    updateICB name ICB { _icbThread = thread, _icbIntf = Nothing, _icbInfo = info }
  infoM "Interfaces.Control" $ "Interface " ++ intf ++ " is now ready for registration"
    where interfaceStartError = failure . InterfaceStartError . show

sendPacket :: Packet -> Interface -> Interface -> IO ()
sendPacket pkt _inputIntf outputIntf@Interface {..} =
  _intfForwarder pkt outputIntf

registerIntf :: Interface -> IO ()
registerIntf intf@Interface{..} = do
  result <- runMaybeT $ do
             lift $ modifyICB _intfName $ setIntf intf
             getICB _intfName
  maybe retry done result
      where retry = do
              infoM "Interfaces.Control" $ show _intfName ++ " not yet ready for registration"
              threadDelay 1000000
              registerIntf intf
            done _ =
              infoM "Interfaces.Control" $ "Interface " ++ show _intfName ++ " has been registered succesfully"

