module Heimdall.CLogging where

import Control.Monad.IO.Class    (MonadIO, liftIO)
import Data.Word                 (Word8)
import Foreign.C.String          (CString, peekCString)
import System.Logging

foreign export ccall cLog :: Word8 -> CString -> CString -> IO ()

cLog :: Word8 -> CString -> CString -> IO ()
cLog 0 = logWith debugM
cLog 1 = logWith infoM
cLog 2 = logWith noticeM
cLog 3 = logWith warningM
cLog 4 = logWith errorM
cLog 5 = logWith criticalM
cLog _ = logWith emergencyM

logWith :: (MonadIO m) => (String -> String -> m ()) -> CString -> CString -> m ()
logWith fun component message = do
  component' <- liftIO $ peekCString component
  message'   <- liftIO $ peekCString message
  fun component' message'
