{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Network.Analysis.HTTP where

import Control.Lens                 ((.=),  (?=), assign, use)
import Control.Monad                (void, when)
import Control.Monad.Trans          (lift)
import Data.ByteString.Char8        (ByteString, isPrefixOf)
import Heimdall.Connections
import Heimdall.Types

import qualified Data.Attoparsec.ByteString.Char8 as A

parseHTTP :: AnalST A.Parser L457Analysis
parseHTTP =
  use direction >>= \case
    Upstream ->
      parseHTTPRequest
    Downstream ->
      parseHTTPResponse
    Unknown ->
      fail "Internal error: unable to determine HTTP direction"

parseHTTPRequest :: AnalST A.Parser L457Analysis
parseHTTPRequest = do
  (method', uri', version') <- parseFirstLine
  analysis . metadata .= HTTPInfo { _method          = method',
                                    _uri             = uri',
                                    _version         = version',
                                    _responseStatus  = Nothing,
                                    _requestHeaders  = [],
                                    _responseHeaders = [] }
  analysis . protocol .= protocolFrom version'
  filterDepth <- use depth
  when (filterDepth > ApplicationLayer) $
    parseHTTPHeaders >>= assign toRequestHeaders
  analysis . status .= Partial
  use analysis
      where protocolFrom str = if isPrefixOf "HTTP" str then HTTP else UnknownProtocol
            toRequestHeaders = analysis . metadata . requestHeaders

parseHTTPHeaders :: AnalST A.Parser [HTTPHeader]
parseHTTPHeaders = return []

parseHTTPResponse :: AnalST A.Parser L457Analysis
parseHTTPResponse = do
  (_version, status', reason') <- parseFirstLine
  analysis . metadata . responseStatus ?= (status', reason')
  filterDepth <- use depth
  when (filterDepth > ApplicationLayer) $
    parseHTTPHeaders >>= assign toResponseHeaders
  analysis . status .= Finished
  use analysis
    where toResponseHeaders = analysis . metadata . responseHeaders

parseFirstLine :: AnalST A.Parser (ByteString, ByteString, ByteString)
parseFirstLine = lift $ do
  A.skipSpace
  first <- takeUntil isSpace
  ws
  second <- takeUntil isSpace
  ws
  third <- takeUntil isCR
  A.skipSpace
  return (first, second, third)

isCR :: Char -> Bool
isCR '\r' = True
isCR _    = False

isSpace :: Char -> Bool
isSpace ' ' = True
isSpace _   = False

takeUntil :: (Char -> Bool) -> A.Parser ByteString
takeUntil predicate = A.takeWhile1 $ not . predicate

ws :: A.Parser ()
ws = void $ A.takeWhile1 isSpace -- faster than skipMany1