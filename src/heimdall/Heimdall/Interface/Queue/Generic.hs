module Heimdall.Interface.Queue.Generic where

import Data.Word                       (Word16)
import Foreign.C.String                (CString)
import Foreign.Ptr                     (Ptr)
import Heimdall.Interface.Queue.Types

foreign import ccall "heimdall.h queue_init"    queue_init    :: Word16 ->
                                                                 CString ->
                                                                 CString ->
                                                                 CString ->
                                                                 IO (Ptr HuvudQueue)

foreign import ccall "heimdall.h queue_destroy" queue_destroy :: Ptr HuvudQueue -> IO ()

platformQueueStart :: IO ()
platformQueueStart = return ()

platformQueueStop :: IO ()
platformQueueStop = return ()

platformQueueInit :: Int -> CString -> CString -> CString -> IO (Int, Ptr HuvudQueue)
platformQueueInit iD component dev filterExp =
    (iD, ) <$> queue_init (fromIntegral iD) component dev filterExp

platformQueueRelease :: (Int, Ptr HuvudQueue) -> IO ()
platformQueueRelease = queue_destroy . snd