{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Stats (Counter(..),
                       StatsField(..),
                       IndexField(..),
                       runStats,
                       increment) where

import Control.Concurrent          (threadDelay)
import Control.Concurrent.Async    (Async, AsyncCancelled, async, cancel)
import Control.Concurrent.MVar     (MVar, newEmptyMVar, putMVar, tryTakeMVar)
import Control.Exception           (Handler(..), SomeException, bracket, catches, throw)
import Control.Monad               (forM_)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (failure, failableIO)
import Control.Monad.Fix           (fix)
import Control.Monad.IO.Class      (MonadIO, liftIO)
import Control.Lens                ((.=), (<<.=), (?=), makeLenses, use)
import Control.Monad.State.Strict  (runState)
import Data.ByteString.Char8       (ByteString, unpack)
import Data.Configurable           (deserialize)
import Data.Count                  (Count, Field, FieldIndex, count, newCountBy)
import Data.Default                (def)
import Data.Fn                     ((-.-))
import Data.IORef                  (IORef, atomicModifyIORef', newIORef)
import Data.IP                     (IP(..), fromHostAddress, fromHostAddress6)
import Data.Ix                     (Ix)
import Data.Tuple                  (swap)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel
import Network.Socket              (SockAddr(..), Family(..), PortNumber)
import Service.Influx              (createService, runService)
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging       hiding (monitor)

import qualified Data.ByteString.Lazy.Char8 as L
import qualified Control.Monad.State.Strict as SS
import qualified Database.Adapter           as DB
import qualified Service.Influx             as Influx

statsKeyPrefix :: ByteString
statsKeyPrefix = "config.heimdall.stats."

data StatsField = RxPackets
                | RxBytes
                | TxPackets
                | TxBytes
                | RoutedPackets
                deriving (Eq, Ix, Ord, Bounded, Enum, Show)

instance Influx.Tag StatsField where
  showTag RxPackets     = "rx-packets"
  showTag RxBytes       = "rx-bytes"
  showTag TxPackets     = "tx-packets"
  showTag TxBytes       = "tx-bytes"
  showTag RoutedPackets = "routed-packets"

instance Influx.Tag IndexField where
  showTag InterfaceField = "intf"
  showTag VRFField       = "vrf"
  showTag PolicyField    = "policy"

instance FieldIndex StatsField

type StatsCount   = Count StatsField Counter IndexField L.ByteString

data IndexField = InterfaceField
                | VRFField
                | PolicyField deriving (Eq, Ord, Show)

data Counter = Zero
             | Add Int
             deriving (Eq, Show)

instance Semigroup Counter where
  (Add x) <> (Add y) = Add (x + y)
  Zero    <> (Add x) = Add x
  (Add x) <> Zero    = Add x
  Zero    <> Zero    = Zero


instance Influx.Tag Counter where
  showTag (Add n)      = L.pack $ show n
  showTag Zero         = ""

instance Monoid Counter where
    mempty = Zero

data State = State {
                _serviceThread :: Maybe (Async ()),
                _counter       :: MVar StatsCount
             }

makeLenses ''State

{-# NOINLINE state #-}
state :: IORef State
state = unsafePerformIO $ do
  zVar <- newEmptyMVar
  newIORef State { _serviceThread = Nothing,
                   _counter       = zVar }

withState :: (MonadIO m) => SS.State State a -> m a
withState = liftIO . atomicModifyIORef' state . swap -.- runState

runStats :: (?env::Env) => IO (Async())
runStats =
    async $ monitor "Stats" [statsKeyPrefix] configChange configChange

configChange :: (?env::Env) => ByteString -> IO ()
configChange key@"config.heimdall.stats.influx-apn" = do
    apn <- DB.lookup db key
    maybe stopService startService apn
  where db = getDB ?env
configChange key = do
  infoM "Stats" $ "Ignoring " ++ unpack key
  return ()

stopService :: IO ()
stopService = do
    infoM "Stats" "Stopping Influx statistics collection"
    zVar   <- newEmptyMVar
    thread <- withState $ do
      counter         .= zVar
      serviceThread <<.= Nothing
    forM_ thread cancel

getIPAddress :: SockAddr -> IP
getIPAddress (SockAddrInet _ addr)      = IPv4 $ fromHostAddress addr
getIPAddress (SockAddrInet6 _ _ addr _) = IPv6 $ fromHostAddress6 addr
getIPAddress _                          = throw $ UnsupportedAddressFamily AF_UNIX

getPort :: SockAddr -> PortNumber
getPort (SockAddrInet port _)      = port
getPort (SockAddrInet6 port _ _ _) = port
getPort _                          = throw $ UnsupportedAddressFamily AF_UNIX

startService :: ByteString -> IO ()
startService apn = do
  stopService
  result <- runExceptT $ do
    sockAddr <- deserialize apn
    let ip   = getIPAddress sockAddr
        port = getPort sockAddr
        config = def { Influx.getHostIP   = ip,
                       Influx.getHostPort = port }
    cVar <- withState $ use counter
    liftIO $ newCountBy Nothing >>= putMVar cVar
    context  <- createService "heimdall-stats" config [cVar]
    failableIO . async . fix $ serviceLoop context
  either failure saveThread result
    where saveThread               = withState . (serviceThread ?=)
          serviceLoop context loop = do
            runService context `catches` [Handler uponCancel, Handler uponError]
            threadDelay 2718281
            loop
          uponCancel (e::AsyncCancelled) = do
            infoM "Stats" $ "Influx service has been canceled"
            throw e
          uponError (e::SomeException) =
            infoM "Stats" $ "Influx service has been terminated upon " ++ show e

increment :: (MonadIO m) => [Field StatsField Counter] -> [Field IndexField L.ByteString] -> m ()
increment fields tags = liftIO $
  withState (use counter) >>= tryWithMVar
    (\c -> count c Nothing fields tags)

tryWithMVar :: (a -> IO ()) -> MVar a -> IO ()
tryWithMVar fun var =
  bracket (tryTakeMVar var) (mapM_ $ putMVar var) (mapM_ fun)























