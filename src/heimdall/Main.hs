module Main where

import Heimdall

import qualified System.Logging as L

main :: IO ()
main = do
  L.init "Heimdall"
  runService
