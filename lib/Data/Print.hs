{-# LANGUAGE TypeSynonymInstances #-}

module Data.Print where

import Data.ByteString.Char8    (ByteString, unpack)
import Data.Binary              (Binary, encode)

import qualified Numeric                    as N
import qualified Data.ByteString.Lazy.Char8 as LB

class Print a where
    toString :: a -> String

instance Print String where
    toString = id

instance Print ByteString where
    toString = unpack

instance Print a => Print (Maybe a) where
  toString = maybe "<none>" toString

showHex :: (Integral a, Binary a) => a -> String
showHex = foldr toHex ""  . LB.unpack . encode
  where toHex = N.showHex . fromEnum
