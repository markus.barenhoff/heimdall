{- |
Description: Heimdall Database Adapter
License: LGPL (See LICENSE)
Maintainer: erick@crankysurfer.com
Copyright: (c) Erick Gonzalez, 2018
-}

module Database.Adapter (Adapter(..),
                         Error(..),
                         Event(..),
                         KeyType(..),
                         SubAction(..),
                         searchAll) where

import Data.Conduit               ((.|), ConduitT, await, runConduit)
import Control.Exception          (Exception)
import Control.Monad.Failable     (Failable)
import Control.Monad.IO.Class     (MonadIO)
import Data.ByteString            (ByteString)
import Data.Typeable              (Typeable)

data Error = DatabaseError String
           | BadData String
           | IOError String
           | NotFound ByteString
           | NoSuchKey ByteString ByteString
           | Status ByteString
           | UnexpectedResult String
             deriving (Show, Typeable)

instance Exception Error

data KeyType = StringType
             | SetType
             | MapType
               deriving Show

data Event = Changed KeyType ByteString
           | Added KeyType ByteString
           | Deleted KeyType ByteString
           | UnknownEvent ByteString
             deriving Show

data SubAction = Done
               | Continue
               | Unsubscribe [ByteString]
               | Subscribe [ByteString]

class Adapter a where
    new        :: (Failable m, MonadIO m) => m a
    get        :: (Failable m, MonadIO m) => a -> ByteString -> m ByteString
    lookup     :: (Failable m, MonadIO m) => a -> ByteString -> m (Maybe ByteString)
    set        :: (Failable m, MonadIO m) => a -> ByteString -> ByteString -> m ()
    del        :: (Failable m, MonadIO m) => a -> [ByteString] -> m ()
    getSet     :: (Failable m, MonadIO m) => a -> ByteString -> m [ByteString]
    addToSet   :: (Failable m, MonadIO m) => a -> ByteString -> [ByteString] -> m ()
    delFromSet :: (Failable m, MonadIO m) => a -> ByteString -> [ByteString] -> m ()
    getMap     :: (Failable m, MonadIO m) => a -> ByteString -> m [(ByteString, ByteString)]
    getMapKV   :: (Failable m, MonadIO m) => a -> ByteString -> ByteString -> m ByteString
    setMapKVs  :: (Failable m, MonadIO m) => a -> ByteString -> [(ByteString, ByteString)] -> m ()
    delMapKVs  :: (Failable m, MonadIO m) => a -> ByteString -> [ByteString] -> m ()
    lookupMap  :: (Failable m, MonadIO m) => a -> ByteString -> ByteString -> m (Maybe ByteString)
    search     :: (Failable m, MonadIO m) => a -> ByteString -> ConduitT () [ByteString] m ()
    uponChange :: (Failable m, MonadIO m) => a -> [ByteString] -> (Event -> IO SubAction) -> m ()

searchAll :: (Failable m, MonadIO m, Adapter a) => a -> ByteString -> m [ByteString]
searchAll adapter pat = runConduit $ search adapter pat .| coalesce []
    where coalesce  acc           = await >>= coalesce' acc
          coalesce' acc (Just []) = coalesce acc
          coalesce' acc (Just r)  = coalesce $ r:acc
          coalesce' acc _         = return $ concat acc
