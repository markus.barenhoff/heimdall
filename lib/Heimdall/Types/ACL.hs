{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.ACL where

import Control.Applicative           ((<|>))
import Control.Lens                  (makeLenses)
import Control.Monad.Failable        (failure)
import Data.ByteString.Char8         (ByteString)
import Data.Char                     (isSpace)
import Data.Configurable             (Configurable(..))
import Data.IP                       (IP)
import Data.Referable                (Referable, VolatileRef)
import Data.Default                  (Default, def)
import Network.Socket                (PortNumber)
import Heimdall.Exceptions
import Heimdall.Types.Connection
import Heimdall.Types.Flow           (Direction)
import Heimdall.Packet               (Protocol)

import qualified Data.Attoparsec.ByteString.Char8 as A

type ACLName = ByteString

data ACL = ACL { aclName  :: ACLName,
                 aclRules :: [ACLRule] }

type ACLRule = (Int, ACE)

newtype PortRange = PortRange (PortNumber, PortNumber)
    deriving Eq

newtype IPRange = IPRange (IP, IP)
    deriving Eq

portInRange :: PortNumber -> PortRange -> Bool
portInRange p (PortRange (start, end)) = p >= start && p <=end

data ACE = ACE { _aceDirection :: Maybe Direction,
                 _aceSrcRange  :: Maybe IPRange,
                 _aceDstRange  :: Maybe IPRange,
                 _aceProtocol  :: Maybe Protocol,
                 _acePorts     :: Maybe PortRange,
                 _aceDevice    :: Maybe String,
                 _aceConnTag   :: Maybe ConnectionTag,
                 _aceAccess    :: Access }
           deriving Eq

data Access = Permit
            | Deny
              deriving Eq

instance Referable ACL

instance Configurable Access where
    serialize Permit = return "permit"
    serialize Deny   = return "deny"
    deserialize      = either (failure . InvalidValue) return . A.parseOnly (permit <|> deny)
        where permit = A.skipWhile isSpace >> A.string "permit" >> return Permit
              deny   = A.skipWhile isSpace >> A.string "deny"   >> return Deny

type ACLRef = VolatileRef ACLName ACL

aclKeyPrefix :: ByteString
aclKeyPrefix = "config.heimdall.acl."

instance Default ACE where
    def = ACE Nothing Nothing Nothing Nothing Nothing Nothing Nothing Deny

makeLenses ''ACE
