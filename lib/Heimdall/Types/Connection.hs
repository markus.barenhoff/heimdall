{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Types.Connection where

import Data.ByteString.Char8         (ByteString)
import Data.Print                    (Print, toString)
import Data.Word                     (Word32)

type ConnectionTag = Word32

connTrackKeyPrefix :: ByteString
connTrackKeyPrefix = "config.heimdall.conntrack."

instance Print ConnectionTag where
  toString = show
