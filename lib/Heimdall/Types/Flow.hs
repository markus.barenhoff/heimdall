module Heimdall.Types.Flow where

import Data.Hashable      (Hashable, hashWithSalt, hash)
import Data.IP            (IP(..), IPv6, ipv4ToIPv6, toHostAddress6)
import Data.Hashes.FNV1a  (fnv1aHash32Base, fnv1aHash32Word32, fnv1aHash32Word16, fnvOffsetBasis32)
import Data.Print         (Print(..))
import Data.Word          (Word8, Word16)
import Heimdall.Packet    (Packet(..), Protocol)

data Flow = IPv6Flow { ip6FlowLeftAddr   :: IPv6,
                       ip6FlowRightAddr  :: IPv6,
                       ip6FlowLeftPort   :: Word16,
                       ip6FlowRightPort  :: Word16,
                       ip6FlowProtocol   :: Word8 }
            deriving (Show, Eq)

data Direction = Upstream
               | Downstream
               | Unknown
               deriving Eq

instance Print Direction where
  toString Upstream   = "upstream"
  toString Downstream = "downstream"
  toString Unknown    = "unknown"

instance Print Flow where
  toString IPv6Flow{..} =
    "Protocol " ++ show ip6FlowProtocol
      ++ ": [" ++ show ip6FlowLeftAddr ++ "]:" ++ show ip6FlowLeftPort
      ++ " <=> [" ++ show ip6FlowRightAddr ++ "]:" ++ show ip6FlowRightPort

instance Hashable Flow where
    -- hash a v6 flow using a Fowler–Noll–Vo hash function, which is supposed to provide
    -- superior uniform distribution at a small performance cost compared to the von Neumann
    -- algorithm as described in RFC 6437
    --
    hashWithSalt salt IPv6Flow{..} =
        let baseHashValue32          = fromIntegral salt
            (srcD, srcC, srcB, srcA) = toHostAddress6 ip6FlowLeftAddr
            (dstD, dstC, dstB, dstA) = toHostAddress6 ip6FlowRightAddr
            addressWords = [srcA, srcB, srcC, srcD, dstA, dstB, dstC, dstD]
            portWords    = [ip6FlowLeftPort, ip6FlowRightPort]
            h1           = foldl fnv1aHash32Word32 baseHashValue32 addressWords
            h2           = fnv1aHash32Base h1 ip6FlowProtocol
            result       = foldl fnv1aHash32Word16 h2 $ fmap fromIntegral portWords
        in case result of
             0 -> 1
             x -> fromIntegral x
    hash = hashWithSalt $ fromIntegral fnvOffsetBasis32

flowFor :: Packet -> Flow
flowFor Packet{..} = getFlow getSrcIP getSrcPort getDstIP getDstPort getProto

getFlow :: IP -> Word16 -> IP -> Word16 -> Protocol -> Flow
getFlow addrA portA addrB portB proto =
    let ((leftAddr, leftPort), (rightAddr, rightPort)) = if addrA < addrB
                                                           then ((addrA, portA), (addrB, portB))
                                                           else ((addrB, portB), (addrA, portA))
        leftAddr'  = v6 leftAddr
        rightAddr' = v6 rightAddr
    in IPv6Flow { ip6FlowLeftAddr  = leftAddr',
                  ip6FlowRightAddr = rightAddr',
                  ip6FlowLeftPort  = leftPort,
                  ip6FlowRightPort = rightPort,
                  ip6FlowProtocol  = fromIntegral $ fromEnum proto }
        where v6 (IPv4 addr) = ipv4ToIPv6 addr
              v6 (IPv6 addr) = addr
