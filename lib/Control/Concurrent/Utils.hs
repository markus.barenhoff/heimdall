module Control.Concurrent.Utils where

import Control.Concurrent           (throwTo, threadDelay)
import Control.Concurrent.Async     (Async, AsyncCancelled(..), asyncThreadId, poll)
import Control.Exception            (SomeException)
import Control.Monad.Fix            (fix)

retryCancel :: Async a -> Int -> IO (Either SomeException a)
retryCancel async waitingPeriod =
    fix retry
        where retry loop = do
                throwTo thread AsyncCancelled
                threadDelay waitingPeriod
                result <- poll async
                maybe loop return result
              thread = asyncThreadId async