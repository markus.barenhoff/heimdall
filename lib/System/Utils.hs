module System.Utils where

import Control.Exception      (bracketOnError)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Int               (Int64)
import System.Clock           (Clock(Monotonic), TimeSpec(..), getTime)
import System.Exit            (ExitCode(..))
import System.Process         (runCommand, terminateProcess, waitForProcess)
import System.Timeout         (timeout)

timeSinceStart :: MonadIO m => m Int64
timeSinceStart = liftIO $
    getTime Monotonic >>= \(TimeSpec secs ns) ->
        return $ secs * 1000000000 + ns

execCommandWithTimeout :: (MonadIO m) => Int -> String -> m (Maybe ExitCode)
execCommandWithTimeout microSecs cmd =
    liftIO $ bracketOnError runCmd terminateCmd $ timeout microSecs . waitForProcess
        where runCmd          = runCommand cmd
              terminateCmd pH = terminateProcess pH >> waitForProcess pH

execCommand :: (MonadIO m) => String -> m (Maybe ExitCode)
execCommand = execCommandWithTimeout maxBound
